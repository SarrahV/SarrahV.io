# How to Style in React and Not Lose Friends

This is an abbreviated version of a 4 Hr workshop I have given at various conferences. We explore the pros and cons of just some of the methods out there, attendees learn to make styling decisions based on the scale of the project and the team involved. By the end, they can feel confident that there is hope! You can style React components without losing friends. 

The slide deck is was made with [reveal-js](https://github.com/hakimel/reveal.js/). To view the slide deck, clone the repo and navigate to the cloned folder. Install the dependencies by running:

`npm install`

Launch the presentation and monitor source files for changes by running:

`npm start`
