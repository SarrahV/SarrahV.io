

import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.section `
	display: 'flex'
`;

const Name = styled.h2 `
	font-size: '1.2em'
`;

const Description = styled.p `
	font-family: 'Lato'; 
	font-size: '1em';
`;

const Img = styled(NormalImg)`
width: auto;
height: 200px;
`;
	

render (
  <Wrapper>
    <Img href={ this.props.imagePath } />
    <div>
      <Name>{ this.props.name }</Name>
      <Description>{ this.props.description }</Description>
    </div>
  </Wrapper>
);


