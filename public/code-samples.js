
import React from 'react';

const styles = {
	friend: {
	  	display: 'flex'
	},
	image: {
		width: 'auto',
		height: '200px'
  },
	name: {
		fontSize:'1.2em',
	},
	description: {
    fontFamily:'Lato', 
    fontSize:'1em',
  }
}

const Friend = () => (
  <div style ={styles.friend}>
    <img href={ this.props.imagePath } style ={styles.image}>
    <div>
      <h2 style ={styles.name}>{ this.props.name }</h2>
      <p style ={styles.description}>{ this.props.description }</p>
    </div>
  </div>
);

export default Friend;




//BEM


import React from 'react';
import './Friend.css';

const Friend = () => (
  <div className="friend friend--best">
    <img href={ this.props.imagePath } className="friend__image">
    <div>
      <h2 className="friend__name">{ this.props.name }</h2>
      <p className="friend__description">{ this.props.description }</p>
    </div>
  </div>
);

export default Friend;